#include "mygameobjectmodel.h"

MyGameObjectModel::MyGameObjectModel(QObject *parent):QAbstractListModel(parent)
{

}

MyGameObjectModel::MyGameObjectModel(gp_game_object gameobject, QObject *parent)
    :QAbstractListModel(parent)
{
    updateGameObjects(gameobject);
}

MyGameObjectModel::~MyGameObjectModel()
{
}

QHash<int, QByteArray> MyGameObjectModel::roleNames() const
{
    QHash<int,QByteArray> roles2;
    roles2[GameObjectDataRoles::GameObjectIDRole] = "gameObjectID";
    roles2[GameObjectDataRoles::GameObjectPositionRole] = "gameObjectPosition";
    return roles2;
}

int MyGameObjectModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _gameObjects.size();
}

QVariant MyGameObjectModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row() >= rowCount() || index.row() < 0)
        return QVariant();
    const auto data = _gameObjects.at(index.row());
    if(role==GameObjectIDRole) return QVariant(data.objectID);
    if(role==GameObjectPositionRole) return QVariant(data.objPos);

    return QVariant();

}

void MyGameObjectModel::updateGameObjects(const gp_game_object &obj)
{
    QPoint loc2(obj.matrix.getM31(),obj.matrix.getM32());
    auto itr = findGameObject(obj.id);
    if(itr<0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _gameObjects.push_back(PrivateModels::GameObjectData(obj.id,loc2));
        endInsertRows();
    }else{
        moveGameObject(obj.id,loc2);
    }
}

void MyGameObjectModel::moveGameObject(const int &objID, const QPoint dir)
{
    auto itr = findGameObject(objID);
    if(itr<0)
        return;
    _gameObjects[itr].objPos = dir;
    emit dataChanged(index(itr),index(itr));
}

void MyGameObjectModel::removeObject(const int &objID)
{
    auto itr = findGameObject(objID);
    if(itr>=0){
        _gameObjects.removeAt(itr);
    }
}

int MyGameObjectModel::findGameObject(const int &objID)
{
    for(int i=0;i<_gameObjects.size();++i)
        if(_gameObjects[i].objectID == objID)
            return i;
    return -1;
}
