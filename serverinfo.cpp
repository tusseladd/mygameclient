#include "serverinfo.h"

ServerInfo::ServerInfo(gp_default_server_query_answer ans, QObject *parent) :
    QObject(parent), _gameConnectPort{ans.server_info.connect_port},
    _playerCount{ans.player_list.count}, _maxSlots{ans.server_info.max_slots},
    _mapWidth{ans.map_info.width},_mapHeight{ans.map_info.height},
    _playerScore{playerHighScore(ans)}
{

}
ServerInfo::ServerInfo(QObject *parent)
    : QObject(parent), _gameConnectPort{0}, _playerCount{0},
      _maxSlots{0}, _mapWidth{0.0}, _mapHeight{0.0}, _playerScore{0}
{

}

ServerInfo::~ServerInfo()
{

}

// Find the highest score on the server
int ServerInfo::playerHighScore(gp_default_server_query_answer ans)
{
    int a = ans.player_list.list[0].score;
    for(uint i=1; i<ans.player_list.count;++i)
        if(ans.player_list.list[i].score > a)
            a=ans.player_list.list[i].score;
    setPlayerScore(a);
    return a;
}

// Get/set methods
uint ServerInfo::gameConnectPort()
{
    return _gameConnectPort;
}

void ServerInfo::setGameConnectPort(uint port)
{
    _gameConnectPort = port;
    emit gameConnectPortChanged();
}

uint ServerInfo::playerCount()
{
    return _playerCount;
}

void ServerInfo::setPlayerCount(uint plyrct)
{
    _playerCount = plyrct;
    emit playerCountChanged();
}

uint ServerInfo::maxSlots()
{
    return _maxSlots;
}

void ServerInfo::setMaxSlots(uint slots_max)
{
    _maxSlots = slots_max;
    emit maxSlotsChanged();
}

double ServerInfo::mapWidth()
{
    return _mapWidth;
}

void ServerInfo::setMapWidth(double width)
{
    _mapWidth = width;
    emit mapWidthChanged();
}

double ServerInfo::mapHeight()
{
    return _mapHeight;
}

void ServerInfo::setMapHeight(double height)
{
    _mapHeight = height;
    emit mapHeightChanged();
}

uint ServerInfo::playerScore()
{
    return _playerScore;
}

void ServerInfo::setPlayerScore(int plyrScore)
{
    _playerScore = plyrScore;
    emit playerScoreChanged();
}
