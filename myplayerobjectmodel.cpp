#include "myplayerobjectmodel.h"

MyPlayerObjectModel::MyPlayerObjectModel(QObject *parent):QAbstractListModel(parent)
{

}

MyPlayerObjectModel::MyPlayerObjectModel(gp_game_object gameobject,bool isEnemy, QObject *parent)
    :QAbstractListModel(parent)
{
    updatePlayerObjects(gameobject,isEnemy);
}

MyPlayerObjectModel::~MyPlayerObjectModel()
{
}

QHash<int, QByteArray> MyPlayerObjectModel::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[PlayerDataRoles::PlayerIDRole] = "playerID";
    //roles[PlayerDataRoles::PlayerNameRole] = "playerName";
    roles[PlayerDataRoles::PlayerPositionRole] = "playerPos";
    roles[PlayerDataRoles::PlayerIsEnemyRole] = "isEnemy";
    return roles;
}

int MyPlayerObjectModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _players.size();
}

QVariant MyPlayerObjectModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row() >= rowCount() || index.row() < 0)
        return QVariant();
    const auto data = _players.at(index.row());
    if(role==PlayerIDRole) return QVariant(data.playerID);
    //if(role==PlayerNameRole) return QVariant(data.playerName);
    if(role==PlayerPositionRole) return QVariant(data.position);
    if(role==PlayerIsEnemyRole) return QVariant(data.isEnemy);
    return QVariant();

}

void MyPlayerObjectModel::updatePlayerObjects(const gp_game_object &obj, bool isEnemy)
{
    QString type(obj.type);
    auto itr = findPlayer(obj.id);
    QPoint loc(obj.matrix.getM31(),obj.matrix.getM32());
    //If the player object doesn't already exist, add it.
    if(itr<0){
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        _players.push_back(PrivateModels::PlayerData(obj.id,loc,isEnemy));
        endInsertRows();
    //If the player object exists, update the object.
    }else if(type=="move"){
        movePlayerObject(obj.id,loc);
    }else if(type=="shoot"){
        emit signalPlayerShot(obj);
    }else if(type=="die"){
        removePlayer(obj.id);
    }else if(type=="spawn"){
        //Basically the default action, included for explanatory purpose.
    }
}

void MyPlayerObjectModel::movePlayerObject(const int &objID, const QPoint dir)
{
    auto itr = findPlayer(objID);
    if(itr<0)
        return;

    _players[itr].position = dir;
    emit dataChanged(index(itr),index(itr));
}

void MyPlayerObjectModel::removePlayer(const int &objID)
{
    auto itr = findPlayer(objID);
    if(itr>=0){
        _players.removeAt(itr);
        return;
    }
}

int MyPlayerObjectModel::findPlayer(const int &objID)
{
    for(int i=0;i<_players.size();++i)
        if(_players[i].playerID == objID)
            return i;
    return -1;
}
