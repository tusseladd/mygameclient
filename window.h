#ifndef WINDOW_H
#define WINDOW_H

#include <QQuickWindow>
#include <QQuickView>
#include <QList>

#include "serverinfo.h"
#include "mytablemodel.h"
#include "mygameobjectmodel.h"
#include "myplayerobjectmodel.h"

class Window : public QQuickView
{
    Q_OBJECT

public:
    explicit Window(QWindow* parent = 0);

signals:
    // GUI signals
    void signalConnectedToLobby();
    void signalConnectedToGame();
    // Game client signals
    void signalButtonPressed(int keyCode);
    void signalSendConnectInfo(QString address,QString port);
    void signalSendJoinInfo(QString address,int gameNumber);
    void signalPlayerNameChanged(QString newname);
    void signalRefreshDSQ();


public slots:
    void handleDSQUpdate(gp_default_server_query_answer &update);
    void handleGameUpdate(gp_game_update update);
    void updateGameObject(const gp_game_object &gameObject);
    void getClientIdent(uint cID);

private:
    void keyPressEvent(QKeyEvent *e);
    QList<QObject*> gameServerList;
    MyTableModel gamesTableModel;
    MyGameObjectModel gameObjectModel;
    MyPlayerObjectModel playerObjectModel;
    uint _myClientID;


};
#endif // WINDOW_H
