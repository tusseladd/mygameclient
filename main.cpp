#include <QCoreApplication>
#include <QtQml>

#include "guiapplication.h"

int main(int argc, char *argv[]){
    GuiApplication a(argc,argv);
    a.initialize();

    return a.exec();
}
