#ifndef MYGAMECLIENT_H
#define MYGAMECLIENT_H

#include <QObject>
#include <QTcpSocket>

#include <gameprotocol.h>

class MyGameClient : public QObject
{
    Q_OBJECT
public:
    explicit MyGameClient(QObject *parent = 0);
    ~MyGameClient();
    QString _playerAlias;

signals:
    void signalHandleGameUpdate(gp_game_update &update);
    void signalHandleDSQAnswer(gp_default_server_query_answer &ans);
    void signalConnectedToLobby(uint myID);
    void signalConnectedToGame();

public slots:
    // Connect and disconnect
    void connectToLobbyServer(const QString &address, int port);
    void connectToGameServer(const QString &address, int port);
    void disconnectFromLobbyServer();
    void disconnectFromGameServer();

    // Other handlers
    void setNewPlayerName(const QString &newName);
    void handleLobbyConnectRequestAnswer(gp_connect_answer &answer);
    void handleConnectButtonClick(const QString &address, QString port);
    void handleJoinGameButtonClick(const QString &addr,int gameNumber);
    void handleKeyPressed(int keyCode);
    void handleErrorResponse(gp_client_error_response &error);
    void handleGameUpdatePacket(gp_game_update &update);
    void handleJoinGameAnswerPacket(gp_join_answer &answer);
    void updateDSQ();

private slots:
    // Data in/out from the server(s)
    void readyToReadFromLobbyServer();
    void readyToReadFromGameServer();

private:
    QTcpSocket *_lobbySocket;
    QTcpSocket *_gameSocket;
    uint _clientID;
    uint _requestID;
    uint _verificationCode;
    QString _gameConnectionIP;
    uint _gameConnectionPort;
    uint _packet_body_size_lobby;
    uint _packet_body_size_game;
    bool _valid_header_read_lobby;
    bool _valid_header_read_game;
    gp_header_prefix _gp_header_template;
    gp_header _gp_header_lobby;
    gp_header _gp_header_game;
    bool _connectedToLobby;
    bool _connectedToGame;
    void sendPacketToServer(int packetType, QTcpSocket *s);
    void sendGameUpdateRequest(gp_game_request &req);

    // Send packet method, generalized for most packets we send.
    template<typename T,typename U> void sendPacket(T& t,U& u,
                                                    QTcpSocket *s)
    {
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_1);
        // DO NOT WANT ;_;
        out.writeRawData(reinterpret_cast<char*>(&t),sizeof(T));
        out.writeRawData(reinterpret_cast<char*>(&u),sizeof(U));
        s->write(block);
    }
    // Send packet method, for bodyless packets.
    template<typename H> void sendPacket(H& h, QTcpSocket *s)
    {
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_1);
        // DO NOT WANT ;_;
        out.writeRawData(reinterpret_cast<char*>(&h),sizeof(H));
        s->write(block);
    }
    // Methods for packet body construction.
    gp_default_server_query constructDefaultServerQuery();
    gp_connect_request constructConnectRequest();
    gp_join_request constructJoinRequest();
    gp_client_verification_answer constructClientVerificationAnswer();

    // Constants defined by evaluating sizes once.
    static const int __CONST_header_size_reduced = sizeof(gp_header)-sizeof(gp_header_prefix);
    static const int __CONST_header_size = sizeof(gp_header);
    static const int __CONST_body_size_connect_answer = sizeof(gp_connect_answer);
    static const int __CONST_body_size_dsq_answer = sizeof(gp_default_server_query_answer);
    static const int __CONST_body_size_error_response = sizeof(gp_client_error_response);
    static const int __CONST_body_size_join_answer = sizeof(gp_join_answer);
    static const int __CONST_body_size_game_update = sizeof(gp_game_update);
};

#endif // MYGAMECLIENT_H
