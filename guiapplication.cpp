#include "guiapplication.h"

GuiApplication::GuiApplication(int &argc, char **argv)
    : QGuiApplication(argc,argv), _window(),_client()
{

}

void GuiApplication::initialize()
{
    // Window signals to client
    QObject::connect(&_window,&Window::signalPlayerNameChanged,
                     &_client,&MyGameClient::setNewPlayerName);
    QObject::connect(&_window,&Window::signalButtonPressed,
                     &_client,&MyGameClient::handleKeyPressed);
    QObject::connect(&_window,&Window::signalSendConnectInfo,
                     &_client,&MyGameClient::handleConnectButtonClick);
    QObject::connect(&_window,&Window::signalSendJoinInfo,
                     &_client,&MyGameClient::handleJoinGameButtonClick);
    // Client signals to window
    QObject::connect(&_client, &MyGameClient::signalConnectedToLobby,
                     &_window, &Window::getClientIdent);
    QObject::connect(&_client, &MyGameClient::signalHandleDSQAnswer,
                     &_window, &Window::handleDSQUpdate);
    QObject::connect(&_client, &MyGameClient::signalConnectedToGame,
                     &_window, &Window::signalConnectedToGame);
    QObject::connect(&_client, &MyGameClient::signalHandleGameUpdate,
                     &_window, &Window::handleGameUpdate);
    // Connect Qt.quit() signal from main.qml
    QObject::connect(reinterpret_cast<QObject*>(_window.engine()),SIGNAL(quit()),
                     this,SLOT(quit()));

    _window.show();
}
