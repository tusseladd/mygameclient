#include "window.h"

#include <QQmlContext>
#include <QQuickItem>

Window::Window(QWindow *parent):QQuickView(parent)
{
    QQmlContext *ctxt = rootContext();
    ctxt->setContextProperty("activeGamesListModel", &gamesTableModel);
    ctxt->setContextProperty("playerModel", &playerObjectModel);
    ctxt->setContextProperty("gameObjectModel", &gameObjectModel);

    setMinimumSize(QSize(800,600));
    setResizeMode(QQuickView::SizeRootObjectToView);
    setSource(QUrl("qrc:/qml/main_qml"));
    setTitle("Game Client - 2D Shooter");
    //Pass on signal that the player wants to connect
    QObject::connect( rootObject(), SIGNAL(sendConnectInfo(QString,QString)),
             this, SIGNAL(signalSendConnectInfo(QString,QString)));
    //Tell the gui that we have connected
    QObject::connect(this,SIGNAL(signalConnectedToLobby()),
            rootObject(), SIGNAL(connectedToLobby()));
    //Signal the client to refresh the server info
    QObject::connect(rootObject(),SIGNAL(refreshDSQ()),
                     this,SIGNAL(signalRefreshDSQ()));
    //Pass on signal that the player wants to join a game
    QObject::connect( rootObject(), SIGNAL(sendJoinInfo(QString,int)),
             this, SIGNAL(signalSendJoinInfo(QString,int)));
    //Tell the gui that we have joined a game
    QObject::connect(this,SIGNAL(signalConnectedToGame()),
            rootObject(),SIGNAL(joinedGame()));
    //Pass on signal that the player has changed his alias
    QObject::connect(rootObject(),SIGNAL(playerNameChanged(QString)),
                     this,SIGNAL(signalPlayerNameChanged(QString)));
    //A player has fired a bullet
    QObject::connect(&playerObjectModel,&MyPlayerObjectModel::signalPlayerShot,
                     this, &Window::updateGameObject);


}

void Window::keyPressEvent(QKeyEvent *e)
{
    emit signalButtonPressed(e->key());
}

void Window::handleGameUpdate(gp_game_update update)
{
    for(uint i=0;i<update.count;++i)
    {
        if(!update.list[i].obj_type==1){
            gameObjectModel.updateGameObjects(update.list[i]);
            qDebug()<<"updated object!";
        }else{
            if(update.list[i].id==_myClientID){
                playerObjectModel.updatePlayerObjects(update.list[i], false);
                qDebug()<<"Updated me!";
            }
            else{
                playerObjectModel.updatePlayerObjects(update.list[i], true);
                qDebug()<<"Updated enemy!";
            }
        }
    }
}

void Window::updateGameObject(const gp_game_object &gameObject)
{
    gameObjectModel.updateGameObjects(gameObject);
}

void Window::getClientIdent(uint cID)
{
    _myClientID = cID;
    emit signalConnectedToLobby();
}

void Window::handleDSQUpdate(gp_default_server_query_answer &update)
{
    gamesTableModel.clearList();
    gamesTableModel.addData(new ServerInfo(update));
    QSize size(update.map_info.width,update.map_info.height);
    setMinimumSize(size);
    resize(size);
}
