#include "mygameclient.h"

MyGameClient::MyGameClient(QObject *parent) :
    QObject(parent),_playerAlias{"player"},_lobbySocket{new QTcpSocket(this)},
    _gameSocket{new QTcpSocket(this)},_clientID{0},_requestID{0},
    _verificationCode{0},_gameConnectionIP{""},_gameConnectionPort{2345},
    _packet_body_size_lobby{0},_packet_body_size_game{0},
    _valid_header_read_lobby{false},_valid_header_read_game{false},
    _connectedToLobby{false},_connectedToGame{false}
{
    QObject::connect(_lobbySocket,&QTcpSocket::readyRead,
                     this,&MyGameClient::readyToReadFromLobbyServer);
    QObject::connect(_gameSocket,&QTcpSocket::readyRead,
                     this,&MyGameClient::readyToReadFromGameServer);
}

MyGameClient::~MyGameClient()
{
    if(_connectedToGame) disconnectFromGameServer();
    _gameSocket->abort();
    _gameSocket->deleteLater();
    if(_connectedToLobby) disconnectFromLobbyServer();
    _lobbySocket->abort();
    _lobbySocket->deleteLater();
}

void MyGameClient::connectToLobbyServer(const QString &address, int port)
{
    _lobbySocket->abort();
    _lobbySocket->connectToHost(address,port);
    if(!_lobbySocket->waitForConnected(1000))
    {
        qDebug()<<"Error: Connection timed out.";
    }else{
        _connectedToLobby=true;
        sendPacketToServer(GP_REQUEST_TYPE_CONNECT,_lobbySocket);
    }
}

void MyGameClient::disconnectFromLobbyServer()
{
    if(_connectedToLobby){
        _lobbySocket->abort();
        _lobbySocket->disconnectFromHost();
        _connectedToLobby=false;
    }
}

void MyGameClient::connectToGameServer(const QString &address, int port)
{
    _gameSocket->abort();
    _gameSocket->connectToHost(address,port);
    if(!_gameSocket->waitForConnected(1000))
    {
        qDebug()<<"Error: Connection timed out.";
    }else{
        _connectedToGame=true;
        emit signalConnectedToGame();
    }
}

void MyGameClient::disconnectFromGameServer()
{
    if(_connectedToGame){
        _gameSocket->abort();
        _gameSocket->disconnectFromHost();
        _connectedToGame=false;
    }
}

void MyGameClient::setNewPlayerName(const QString &newName)
{
    _playerAlias=newName;
}

void MyGameClient::handleLobbyConnectRequestAnswer(gp_connect_answer &answer)
{
    if (answer.state){
        _clientID = answer.client_id;        
        emit signalConnectedToLobby(_clientID);
        sendPacketToServer(GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY,
                           _lobbySocket);
    }else{
        qDebug()<<"Error: Connection refused.";
    }
}

void MyGameClient::sendPacketToServer(int packetType,QTcpSocket *s)
{
    gp_header head;
    head.size = __CONST_header_size_reduced;
    head.type = packetType;
    head.flags.answer=0;
    switch(packetType){
    case GP_REQUEST_TYPE_CONNECT:{
        head.request_id = ++_requestID;
        gp_connect_request req = constructConnectRequest();
        sendPacket(head,req,s);
    }break;
    case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:{
        head.request_id = ++_requestID;
        gp_default_server_query query = constructDefaultServerQuery();
        sendPacket(head,query,s);
    }break;
    case GP_REQUEST_TYPE_JOIN:{
        head.request_id = ++_requestID;
        gp_join_request request = constructJoinRequest();
        sendPacket(head,request,s);
    }break;
    case GP_REQUEST_TYPE_CLIENT_VERIFICATION:{
        head.request_id = _requestID;
        head.flags.answer = 1;
        gp_client_verification_answer ans = constructClientVerificationAnswer();
        sendPacket(head,ans,s);
    }break;
    case GP_REQUEST_TYPE_PING:{
        head.request_id = ++_requestID;
        sendPacket(head,s);
    }break;
    default:
        qDebug()<<"Error: Invalid header type.";
        return;
    }
}

gp_default_server_query MyGameClient::constructDefaultServerQuery()
{
    gp_default_server_query dsq;
    dsq.request_flags.server_info = true;
    dsq.request_flags.player_list = true;
    dsq.request_flags.map_info = true;
    return dsq;
}

gp_connect_request MyGameClient::constructConnectRequest()
{
    gp_connect_request req;
    req.connect_flag = GP_CONNECT_FLAG_CONNECT;
    return req;
}

gp_join_request MyGameClient::constructJoinRequest()
{
    gp_join_request req;
    req.client_id = _clientID;
    req.flags.join_flag = GP_JOIN_FLAG_JOIN;
    req.flags.client_type = GP_GAME_OBJECT_TYPE_PLAYER;
    strcpy(req.player_name, _playerAlias.toStdString().c_str());
    return req;
}

gp_client_verification_answer MyGameClient::constructClientVerificationAnswer()
{
    gp_client_verification_answer ans;
    ans.client_id = _clientID;
    ans.vcode = _verificationCode;
    return ans;
}

void MyGameClient::sendGameUpdateRequest(gp_game_request &req)
{
    gp_header head;
    head.size = __CONST_header_size_reduced;
    head.type = GP_REQUEST_TYPE_GAME;
    head.flags.answer = 0;
    head.request_id = ++_requestID;

    sendPacket(head,req,_gameSocket);
}

// The GUI connect button has been clicked
void MyGameClient::handleConnectButtonClick(const QString &address, QString port)
{
    if(port.toInt()!=0)
        connectToLobbyServer(address,port.toInt());
    else
        connectToLobbyServer(address,1234);
}

// The GUI join game button has been clicked
void MyGameClient::handleJoinGameButtonClick(const QString &addr,int gameNumber)
{
    sendPacketToServer(GP_REQUEST_TYPE_JOIN,_lobbySocket);
    _gameConnectionIP = addr;
    _gameConnectionPort = gameNumber;
}

// Handle keypress events.
void MyGameClient::handleKeyPressed(int keyCode)
{
    gp_game_request req;

    QString command;
    QVector<QString> vec;
    switch (keyCode){
    case Qt::Key_Up:
        command = "move";
        vec.push_back("0");
        vec.push_back("-10");
        break;
    case Qt::Key_Down:
        command = "move";
        vec.push_back("0");
        vec.push_back("10");
        break;
    case Qt::Key_Left:
        command = "move";
        vec.push_back("-10");
        vec.push_back("0");
        break;
    case Qt::Key_Right:
        command = "move";
        vec.push_back("10");
        vec.push_back("0");
        break;
    case Qt::Key_Space:
        command = "shoot";
        vec.push_back("0");
        vec.push_back("-10");
        break;
    default:
        return;

    }
    strcpy(req.cmd,command.toStdString().c_str());
    req.param_count = vec.size();

    int i=0;
    for (const auto &x : vec)
        strcpy(req.params[i++].param,x.toStdString().c_str());

    sendGameUpdateRequest(req);
}

void MyGameClient::handleErrorResponse(gp_client_error_response &error)
{
    qDebug()<<"Error: " << error.error;
}

void MyGameClient::handleGameUpdatePacket(gp_game_update &update)
{
    emit signalHandleGameUpdate(update);
}

void MyGameClient::handleJoinGameAnswerPacket(gp_join_answer &answer)
{
    if(answer.state == 1){
        _verificationCode = answer.validation_code;
        connectToGameServer(_gameConnectionIP,_gameConnectionPort);
    }
    else
        qDebug()<<"Disconnected.";
}

//Used by the 'Refresh' button in the GUI.
void MyGameClient::updateDSQ()
{
    sendPacketToServer(GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY,_lobbySocket);
}

void MyGameClient::readyToReadFromLobbyServer()
{
    QDataStream in(_lobbySocket);
    in.setVersion(QDataStream::Qt_5_1);

    //Do we have a valid header? MARK: _valid_header_read will
    //be false until a valid header has been read.
    if(!_valid_header_read_lobby)
    {
        //Reset body size, ready for new body
        _packet_body_size_lobby=0;

        //Do we have enough data to read header? If not, get more.
        if(_lobbySocket->bytesAvailable()<__CONST_header_size)
            return;

        //Read header
        in.readRawData(reinterpret_cast<char*>(&_gp_header_lobby),__CONST_header_size);

        //Does the header prefix match our template? If not;
        //not our package, dismiss and get more data.
        if( _gp_header_lobby.prefix.id != _gp_header_template.id )
          return;

        switch(_gp_header_lobby.type)
        {
        case GP_REQUEST_TYPE_CONNECT:
            _packet_body_size_lobby=__CONST_body_size_connect_answer;
            _valid_header_read_lobby=true;
            qDebug()<<"connected";
            break;

        case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
            _packet_body_size_lobby=__CONST_body_size_dsq_answer;
            _valid_header_read_lobby=true;
            qDebug()<<"DSQ GET!";
            break;

        case GP_REQUEST_TYPE_ERROR:
            _packet_body_size_lobby=__CONST_body_size_error_response;
            _valid_header_read_lobby=true;
            qDebug()<<"Error got :(";
            break;

        case GP_REQUEST_TYPE_JOIN:
            _packet_body_size_lobby=__CONST_body_size_join_answer;
            _valid_header_read_lobby=true;
            qDebug()<<"Join answer got.";
            break;

        case GP_REQUEST_TYPE_PING:
            //Body size should still be 0
            _valid_header_read_lobby = true;
            break;

        default:
            return;
        }
    }
    //Do we have enough data to process the body of this packet?
    //If not, get more data.
    if(_lobbySocket->bytesAvailable()<_packet_body_size_lobby)
        return;

    //Aquire the packet body (if applicable)
    switch(_gp_header_lobby.type)
    {
    case GP_REQUEST_TYPE_ERROR:
    {
        gp_client_error_response answer;
        in.readRawData( reinterpret_cast<char*>(&answer), __CONST_body_size_error_response );
        handleErrorResponse(answer);
    } break;

    case GP_REQUEST_TYPE_JOIN:
    {
        gp_join_answer answer;
        in.readRawData( reinterpret_cast<char*>(&answer),__CONST_body_size_join_answer);
        handleJoinGameAnswerPacket(answer);
    }break;

    case GP_REQUEST_TYPE_CONNECT:
    {
        gp_connect_answer answer;
        in.readRawData(reinterpret_cast<char*>(&answer),__CONST_body_size_connect_answer);
        handleLobbyConnectRequestAnswer(answer);
    } break;

    case GP_REQUEST_TYPE_DEFAULT_SERVER_QUERY:
    {
        gp_default_server_query_answer answer;
        in.readRawData(reinterpret_cast<char*>(&answer),__CONST_body_size_dsq_answer);
        emit signalHandleDSQAnswer(answer);
    } break;

    case GP_REQUEST_TYPE_PING:
    {
        qDebug()<<"Pong!";
    } break;
    default:
        return;
    }

    // Reset the valid header state so we can look for new packets
    _valid_header_read_lobby = false;

    //If buffer data available is greater or equal to
    //the header size, look for extra packets.
    if( _lobbySocket->bytesAvailable() >= __CONST_header_size )
      readyToReadFromLobbyServer();
}

void MyGameClient::readyToReadFromGameServer()
{
    QDataStream in(_gameSocket);
    in.setVersion(QDataStream::Qt_5_1); //server version

    //Not read header yet? Then we probably should.
    if(!_valid_header_read_game)
    {
        //New header, new body
        _packet_body_size_game = 0;

        //Do we have enough data to read a header?
        //If not, get more.
        if(_gameSocket->bytesAvailable()<__CONST_header_size)
            return;

        //Get header from buffer
        in.readRawData(reinterpret_cast<char*>(&_gp_header_game),__CONST_header_size);

        //Is this our packet? If not, return for one of ours.
        if( _gp_header_game.prefix.id != _gp_header_template.id )
          return;

        switch(_gp_header_game.type)
        {
        case GP_REQUEST_TYPE_GAME:
            _packet_body_size_game = __CONST_body_size_game_update;
            _valid_header_read_game = true;
            break;

        case GP_REQUEST_TYPE_ERROR:
            _packet_body_size_game = __CONST_body_size_error_response;
            _valid_header_read_game = true;
            qDebug()<<"Game error got.";
            break;

        case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
            //_body_size_game = 0; Should be 0 anyway
            _valid_header_read_game = true;
            qDebug()<<"Client verification request.";
            break;

        default:
            return;
        }
    }
    //Enough data to get the whole body (if there is one)?
    //If not, get more data.
    if(_gameSocket->bytesAvailable()<_packet_body_size_game)
        return;

    //Get the packet body from buffer
    switch(_gp_header_game.type)
    {
    case GP_REQUEST_TYPE_GAME:
        gp_game_update update;
        in.readRawData(reinterpret_cast<char*>(&update),__CONST_body_size_game_update);
        handleGameUpdatePacket(update);
        qDebug()<<"Game update got.";
        break;

    case GP_REQUEST_TYPE_CLIENT_VERIFICATION:
        sendPacketToServer(GP_REQUEST_TYPE_CLIENT_VERIFICATION,_gameSocket);
        qDebug()<<"Client verification sent.";
        break;

    case GP_REQUEST_TYPE_ERROR:
        gp_client_error_response error;
        in.readRawData(reinterpret_cast<char*>(&error),__CONST_body_size_error_response);
        handleErrorResponse(error);
        qDebug()<<"Error: " << error.error;
        break;

    default:
        return;
    }

    //Reset variable to allow reading new packets
    _valid_header_read_game = false;

    //Do we have enough data on the buffer to
    //read another packet header?
    if(_gameSocket->bytesAvailable()>= __CONST_header_size)
        readyToReadFromGameServer();
}
