import QtQuick 2.3
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2

// The menu bar posed some problems...
// http://stackoverflow.com/questions/19858941/use-menubar-component-in-a-qquickview-on-windows

Item{
    id:root
    property string player_name: "player"
    signal playerNameChanged(string name)
    signal sendConnectInfo(string address,string port)
    signal sendJoinInfo(string address,int gameNumber)
    signal connectedToLobby()
    signal refreshDSQ()
    signal joinedGame()
    state: "defaultState"
    onConnectedToLobby: {connectionStatusLabelText.text = "Connected.";}
    onJoinedGame: {state = "joinedGameState";
                    connectdialog.close();
    }

    // Connect dialog, handles connecting to server(s)
    // and joining games
    Dialog{
        id: connectdialog
        title: "Connect to server"
        width: 320
        height: 400

        Label {
            id:lobbyAddressLabel
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 30
            text: "Server address:"
        }
        // Specify server address
        TextField {
            id: lobbyAddressField
            anchors.top: lobbyAddressLabel.bottom
            anchors.left: lobbyAddressLabel.left
            inputMask: "000.000.000.000;_"
        }
        Label {
            id:lobbyPortLabel
            anchors.left: lobbyAddressField.right
            anchors.top: lobbyAddressLabel.top
            anchors.leftMargin: 10
            text: "Port:"
        }
        // Specify server port
        TextField {
            id: lobbyPortField
            anchors.left: lobbyAddressField.right
            anchors.top: lobbyPortLabel.bottom
            anchors.leftMargin: 10
            placeholderText: "1234"
            inputMask: "9999"

        }

        // Is updated based on connection status
        Label {
            id:connectionStatusLabel
            anchors.left: lobbyAddressField.left
            anchors.top: lobbyAddressField.bottom
            anchors.topMargin: 10
            Text{
                id:connectionStatusLabelText
                text: "Not connected."
            }
        }
        // Connect to server
        Button {
            id: connectToLobbyButton
            anchors.right: lobbyPortField.right
            anchors.top: lobbyPortField.bottom
            anchors.topMargin: 10
            text: "Connect"
            onClicked: sendConnectInfo(lobbyAddressField.text,
                                       lobbyPortField.text)

        }

        Label {
            id: activeGamesListLabel
            text: "Games currently running:"
            anchors.left: lobbyAddressLabel.left
            anchors.top: connectToLobbyButton.bottom
            anchors.topMargin: 15
        }

        // Tableview to list all running games
        TableView {
            id: activeGamesTable
            anchors.left: activeGamesListLabel.left
            anchors.top: activeGamesListLabel.bottom

            TableViewColumn {
                role: "gameID"
                title: "ID"
                width: 50
            }
            TableViewColumn {
                role: "playerCount"
                title: "Players"
                width: 50
            }
            TableViewColumn {
                role: "playerScore"
                title: "HiScore"
                width: 50
            }
            model: activeGamesListModel
        }
        // Join selected game (from list)
        Button {
            id: joinGameButton
            text: "Join game"
            anchors.right: activeGamesTable.right
            anchors.topMargin: 10
            anchors.top: activeGamesTable.bottom
            onClicked: sendJoinInfo(lobbyAddressField.text,
                                    2345)
        }
        // Refresh active games
        Button {
            id: refreshGamesInfoButton
            text: "Refresh"
            anchors.right: joinGameButton.left
            anchors.rightMargin: 10
            anchors.top: joinGameButton.top
            onClicked: refreshDSQ();
        }
    }

    // Change player name
    Dialog{
        id: playerNameDialog
        title: "Player name"
        width: 200
        height: 100

        TextField{
            id: playerNameField
            maximumLength: 64
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 20
            placeholderText: player_name
            validator: RegExpValidator {regExp: /\w+/}
        }
        Button{
            id: playerNameButton
            text:"OK"
            anchors.horizontalCenter: playerNameField.horizontalCenter
            anchors.top: playerNameField.bottom
            anchors.topMargin: 10
            onClicked:{
                    player_name = playerNameField.text;
                    playerNameChanged(player_name);
                    playerNameDialog.close();
            }
        }
    }
    // About the client
    Dialog{
        id: aboutDialog
        title: "About"
        width: 200
        height: 100

        Label{
            id: aboutLabel
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 20
            text:"Client by Stian Ottesen (2014).\n\nSprites by MillionthVector.\nhttp://millionthvector.blogspot.no/"
        }
        Button{
            id: aboutButton
            text:"OK"
            anchors.horizontalCenter: aboutLabel.horizontalCenter
            anchors.top: aboutLabel.bottom
            anchors.topMargin: 10
            onClicked:{aboutDialog.close();}
        }
    }

    // The wrapper for the menu bar, needed to make the bar
    // show up on the Windows platform
    Item{
        id:menuwrapper
        anchors.fill: parent
        // The menu bar
        MenuBar{
            id: mymenubar
            Menu {
                title: "File"
                MenuItem { text: "Connect..."
                            onTriggered: connectdialog.open();
                }
                MenuItem { text: "Exit"
                            onTriggered: Qt.quit();
                }
            }
            Menu {
                title: "Options"
                MenuItem { text: "Player info"
                            onTriggered: playerNameDialog.open();
                }
                // NYI - file IO etc
                MenuItem { text: "Key bindings" }
                // NYI - change between styles
                MenuItem { text: "Theme" }
            }
            Menu {
                title: "Help"
                MenuItem { text: "About"
                            onTriggered:aboutDialog.open();}
            }
        }

        // This will make sure the menubar shows up
        // both on Windows and OSX (somehow it
        // works only on OSX without the wrapper)
        states: State {
            name: "hasMenuBar"
            when: mymenubar && !mymenubar.__isNative

            ParentChange{
                target: mymenubar.__contentItem
                parent: root
            }
            PropertyChanges {
                target: mymenubar.__contentItem
                x: 0
                y: 0
                width: menuwrapper.width
            }
        }
    }

    // Background
    Rectangle{
        id: rootViewBackground
        color: "#66bbBB"
        anchors.fill: parent
        Image{
            id:rootViewBackGroundImage
            anchors.fill: parent
            source:"/img/bgtwo"
        }

    }
    // Player objects
    Component{
        id: player_visualization_delegate
        RowLayout{
            id: player
            x: playerPos.x
            y: playerPos.y
            Image{id:playerImage
                width:20
                height:20
                fillMode: Image.Stretch //GRRRR
                source:isEnemy?"/img/imgtwo":"/img/imgone"
                asynchronous: true
                anchors.centerIn: parent
                clip:true
            }
        }
    }
    // Bullets
    Component{
        id: bullet_visualization_delegate
        RowLayout{
            id: bullet
            x: gameObjectPosition.x
            y: gameObjectPosition.y

            Rectangle{width:5;height:5;color:"white"}
        }
    }

    // Game window
    Rectangle{
        id: gameViewRectangle
        anchors.fill: parent
        color: "Green"
        Image{
            id:rootBackGroundImage
            anchors.fill: parent
            source:"/img/bgone"
        }
        Repeater{
            anchors.fill: parent
            model:VisualDataModel{
                model:gameObjectModel
                delegate: bullet_visualization_delegate
            }
        }

        Repeater{
            anchors.fill: parent
            model:VisualDataModel{
                model:playerModel
                delegate: player_visualization_delegate
            }
        }
    }

    // Background states
    states:[ State{
        name: "defaultState"
        PropertyChanges {
            target: rootViewBackground
            visible: true
        }
        PropertyChanges{
            target: gameViewRectangle
            visible: false
        }
    },
    State{
        name: "joinedGameState"
        PropertyChanges {
            target: rootViewBackground
            visible: false
        }
        PropertyChanges{
            target: gameViewRectangle
            visible: true
        }
    }]

}
