#ifndef MYGAMEOBJECTMODEL_H
#define MYGAMEOBJECTMODEL_H

#include <QObject>
#include <QTransform>
#include <QMatrix>
#include <QAbstractListModel>

#include <algorithm>

#include "gameprotocol.h"

namespace PrivateModels{
    // Struct for holding game objects
    struct GameObjectData{
        explicit GameObjectData():objectID{},objPos{}{}
        explicit GameObjectData(int oid,QPoint pos):
            objectID{oid},objPos{pos}{}
        int objectID;
        QPoint objPos;
    };
}
class MyGameObjectModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum GameObjectDataRoles:int{
        GameObjectIDRole = Qt::UserRole+1,
        GameObjectPositionRole
    };
    explicit MyGameObjectModel(QObject *parent = 0);
    explicit MyGameObjectModel(gp_game_object gameobject,
                               QObject *parent = 0);
    ~MyGameObjectModel();

    QHash<int,QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    void updateGameObjects(const gp_game_object &obj);
    void moveGameObject(const int &objID, const QPoint dir);
    void removeObject(const int &objID);

private:
    int findGameObject(const int &objID);
    QVector<PrivateModels::GameObjectData> _gameObjects;

};
#endif // MYGAMEOBJECTMODEL_H
