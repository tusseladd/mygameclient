#ifndef SERVERINFO_H
#define SERVERINFO_H

#include <QObject>

#include <gameprotocol.h>

class ServerInfo : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(QString hostname READ hostname WRITE setHostname)
    Q_PROPERTY(uint gameConnectPort READ gameConnectPort WRITE setGameConnectPort NOTIFY gameConnectPortChanged)
    Q_PROPERTY(uint playerCount READ playerCount WRITE setPlayerCount NOTIFY playerCountChanged)
    Q_PROPERTY(uint maxSlots READ maxSlots WRITE setMaxSlots NOTIFY maxSlotsChanged)
    Q_PROPERTY(double mapWidth READ mapWidth WRITE setMapWidth NOTIFY mapWidthChanged)
    Q_PROPERTY(double mapHeight READ mapHeight WRITE setMapHeight NOTIFY mapHeightChanged)
    Q_PROPERTY(int playerScore READ playerScore WRITE setPlayerScore NOTIFY playerScoreChanged)

public:
    ServerInfo(gp_default_server_query_answer ans, QObject *parent = 0);
    ServerInfo(QObject *parent = 0);
    virtual ~ServerInfo();

    // Q_PROPERTY get/set methods
    uint gameConnectPort();
    void setGameConnectPort(uint port);
    uint playerCount();
    void setPlayerCount(uint plyrct);
    uint maxSlots();
    void setMaxSlots(uint slots_max);
    double mapWidth();
    void setMapWidth(double width);
    double mapHeight();
    void setMapHeight(double height);
    uint playerScore();
    void setPlayerScore(int plyrScore);
    int playerHighScore(gp_default_server_query_answer ans);

signals:
    // Q_PROPERTY signals
    void gameConnectPortChanged();
    void playerCountChanged();
    void maxSlotsChanged();
    void mapWidthChanged();
    void mapHeightChanged();
    void playerScoreChanged();

public slots:

private:
    uint _gameConnectPort;
    uint _playerCount;
    uint _maxSlots;
    double _mapWidth;
    double _mapHeight;
    int _playerScore;

};
#endif // SERVERINFO_H
