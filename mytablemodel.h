#ifndef MYTABLEMODEL_H
#define MYTABLEMODEL_H

#include <QAbstractTableModel>

#include "serverinfo.h"

// Model class for the QML tableview
class MyTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum DataRoles:int{
        GameIDRole = Qt::UserRole + 1,
        PlayerCountRole,
        PlayerScoreRole
    };
    explicit MyTableModel(QObject *parent = 0);
    explicit MyTableModel(ServerInfo *serverlist,QObject *parent = 0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool removeRows(int row, int count, const QModelIndex &parent) override;
    bool addData(ServerInfo *inf);
    bool clearList();
    QList<ServerInfo *> getList();
    QHash<int,QByteArray> roleNames() const override;

public slots:

private:
    QList<ServerInfo*> _gameList;

};

#endif // MYTABLEMODEL_H
