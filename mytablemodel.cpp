#include "mytablemodel.h"

MyTableModel::MyTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

MyTableModel::MyTableModel(ServerInfo *serverlist, QObject *parent) :
    QAbstractTableModel(parent)
{
    _gameList.append(serverlist);
}

int MyTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (_gameList.size()>0)
        return _gameList.size();
    return 0;
}

int MyTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant MyTableModel::data(const QModelIndex &index, int role) const
{

    if(!index.isValid())
        return QVariant();
    if(index.row() >= _gameList.size() || index.row() < 0)
        return QVariant();
    ServerInfo *data = _gameList.at(index.row());
    if(role==GameIDRole) return QVariant(data->gameConnectPort());
    if(role==PlayerCountRole) return QVariant(data->playerCount());
    if(role==PlayerScoreRole) return QVariant(data->playerScore());

    return QVariant();
}

bool MyTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(),row,row+count-1);
    for(int i=0;i<count;++i){
        _gameList.removeAt(row);
    }
    return true;
}

bool MyTableModel::addData(ServerInfo *inf)
{
    beginInsertRows(QModelIndex(),_gameList.size(),_gameList.size());
    _gameList.append(inf);
    endInsertRows();
    return true;
}

bool MyTableModel::clearList()
{
    beginRemoveRows(QModelIndex(),0,_gameList.size());
    _gameList.clear();
    endRemoveRows();
    return true;
}

QList<ServerInfo*> MyTableModel::getList()
{
    return _gameList;
}

QHash<int, QByteArray> MyTableModel::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[DataRoles::GameIDRole] = "gameID";
    roles[DataRoles::PlayerCountRole] = "playerCount";
    roles[DataRoles::PlayerScoreRole] = "playerScore";
    return roles;
}
