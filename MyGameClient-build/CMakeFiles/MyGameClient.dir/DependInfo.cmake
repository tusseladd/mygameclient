# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Game Design/Projects/MyGameClient/guiapplication.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/guiapplication.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/main.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/main.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_guiapplication.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_guiapplication.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_mygameclient.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_mygameclient.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_mygameobjectmodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_mygameobjectmodel.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_myplayerobjectmodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_myplayerobjectmodel.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_mytablemodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_mytablemodel.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_serverinfo.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_serverinfo.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/moc_window.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/moc_window.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/mygameclient.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/mygameclient.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/mygameobjectmodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/mygameobjectmodel.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/myplayerobjectmodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/myplayerobjectmodel.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/mytablemodel.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/mytablemodel.cpp.obj"
  "D:/Game Design/Builds/MyGameClient-build/qrc_myresources.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/qrc_myresources.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/serverinfo.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/serverinfo.cpp.obj"
  "D:/Game Design/Projects/MyGameClient/window.cpp" "D:/Game Design/Builds/MyGameClient-build/CMakeFiles/MyGameClient.dir/window.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtQuick"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include/QtQuick"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtQml"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include/QtQml"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtNetwork"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtCore"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/mkspecs/win32-g++"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtGui"
  "D:/Game Design/Projects/MyGameClient/gameprotocol"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
