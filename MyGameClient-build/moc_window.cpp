/****************************************************************************
** Meta object code from reading C++ file 'window.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Projects/MyGameClient/window.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Window_t {
    QByteArrayData data[24];
    char stringdata[344];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Window_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Window_t qt_meta_stringdata_Window = {
    {
QT_MOC_LITERAL(0, 0, 6),
QT_MOC_LITERAL(1, 7, 22),
QT_MOC_LITERAL(2, 30, 0),
QT_MOC_LITERAL(3, 31, 21),
QT_MOC_LITERAL(4, 53, 19),
QT_MOC_LITERAL(5, 73, 7),
QT_MOC_LITERAL(6, 81, 21),
QT_MOC_LITERAL(7, 103, 7),
QT_MOC_LITERAL(8, 111, 4),
QT_MOC_LITERAL(9, 116, 18),
QT_MOC_LITERAL(10, 135, 10),
QT_MOC_LITERAL(11, 146, 23),
QT_MOC_LITERAL(12, 170, 7),
QT_MOC_LITERAL(13, 178, 16),
QT_MOC_LITERAL(14, 195, 15),
QT_MOC_LITERAL(15, 211, 31),
QT_MOC_LITERAL(16, 243, 6),
QT_MOC_LITERAL(17, 250, 16),
QT_MOC_LITERAL(18, 267, 14),
QT_MOC_LITERAL(19, 282, 16),
QT_MOC_LITERAL(20, 299, 14),
QT_MOC_LITERAL(21, 314, 10),
QT_MOC_LITERAL(22, 325, 14),
QT_MOC_LITERAL(23, 340, 3)
    },
    "Window\0signalConnectedToLobby\0\0"
    "signalConnectedToGame\0signalButtonPressed\0"
    "keyCode\0signalSendConnectInfo\0address\0"
    "port\0signalSendJoinInfo\0gameNumber\0"
    "signalPlayerNameChanged\0newname\0"
    "signalRefreshDSQ\0handleDSQUpdate\0"
    "gp_default_server_query_answer&\0update\0"
    "handleGameUpdate\0gp_game_update\0"
    "updateGameObject\0gp_game_object\0"
    "gameObject\0getClientIdent\0cID"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Window[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    0,   70,    2, 0x06 /* Public */,
       4,    1,   71,    2, 0x06 /* Public */,
       6,    2,   74,    2, 0x06 /* Public */,
       9,    2,   79,    2, 0x06 /* Public */,
      11,    1,   84,    2, 0x06 /* Public */,
      13,    0,   87,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,   88,    2, 0x0a /* Public */,
      17,    1,   91,    2, 0x0a /* Public */,
      19,    1,   94,    2, 0x0a /* Public */,
      22,    1,   97,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    8,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    7,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 18,   16,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, QMetaType::UInt,   23,

       0        // eod
};

void Window::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Window *_t = static_cast<Window *>(_o);
        switch (_id) {
        case 0: _t->signalConnectedToLobby(); break;
        case 1: _t->signalConnectedToGame(); break;
        case 2: _t->signalButtonPressed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->signalSendConnectInfo((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->signalSendJoinInfo((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->signalPlayerNameChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->signalRefreshDSQ(); break;
        case 7: _t->handleDSQUpdate((*reinterpret_cast< gp_default_server_query_answer(*)>(_a[1]))); break;
        case 8: _t->handleGameUpdate((*reinterpret_cast< gp_game_update(*)>(_a[1]))); break;
        case 9: _t->updateGameObject((*reinterpret_cast< const gp_game_object(*)>(_a[1]))); break;
        case 10: _t->getClientIdent((*reinterpret_cast< uint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Window::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalConnectedToLobby)) {
                *result = 0;
            }
        }
        {
            typedef void (Window::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalConnectedToGame)) {
                *result = 1;
            }
        }
        {
            typedef void (Window::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalButtonPressed)) {
                *result = 2;
            }
        }
        {
            typedef void (Window::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalSendConnectInfo)) {
                *result = 3;
            }
        }
        {
            typedef void (Window::*_t)(QString , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalSendJoinInfo)) {
                *result = 4;
            }
        }
        {
            typedef void (Window::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalPlayerNameChanged)) {
                *result = 5;
            }
        }
        {
            typedef void (Window::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Window::signalRefreshDSQ)) {
                *result = 6;
            }
        }
    }
}

const QMetaObject Window::staticMetaObject = {
    { &QQuickView::staticMetaObject, qt_meta_stringdata_Window.data,
      qt_meta_data_Window,  qt_static_metacall, 0, 0}
};


const QMetaObject *Window::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Window::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Window.stringdata))
        return static_cast<void*>(const_cast< Window*>(this));
    return QQuickView::qt_metacast(_clname);
}

int Window::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Window::signalConnectedToLobby()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Window::signalConnectedToGame()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void Window::signalButtonPressed(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Window::signalSendConnectInfo(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Window::signalSendJoinInfo(QString _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Window::signalPlayerNameChanged(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Window::signalRefreshDSQ()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}
QT_END_MOC_NAMESPACE
