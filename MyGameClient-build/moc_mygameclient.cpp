/****************************************************************************
** Meta object code from reading C++ file 'mygameclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Projects/MyGameClient/mygameclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mygameclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MyGameClient_t {
    QByteArrayData data[37];
    char stringdata[629];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MyGameClient_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MyGameClient_t qt_meta_stringdata_MyGameClient = {
    {
QT_MOC_LITERAL(0, 0, 12),
QT_MOC_LITERAL(1, 13, 22),
QT_MOC_LITERAL(2, 36, 0),
QT_MOC_LITERAL(3, 37, 15),
QT_MOC_LITERAL(4, 53, 6),
QT_MOC_LITERAL(5, 60, 21),
QT_MOC_LITERAL(6, 82, 31),
QT_MOC_LITERAL(7, 114, 3),
QT_MOC_LITERAL(8, 118, 22),
QT_MOC_LITERAL(9, 141, 4),
QT_MOC_LITERAL(10, 146, 21),
QT_MOC_LITERAL(11, 168, 20),
QT_MOC_LITERAL(12, 189, 7),
QT_MOC_LITERAL(13, 197, 4),
QT_MOC_LITERAL(14, 202, 19),
QT_MOC_LITERAL(15, 222, 25),
QT_MOC_LITERAL(16, 248, 24),
QT_MOC_LITERAL(17, 273, 16),
QT_MOC_LITERAL(18, 290, 7),
QT_MOC_LITERAL(19, 298, 31),
QT_MOC_LITERAL(20, 330, 18),
QT_MOC_LITERAL(21, 349, 6),
QT_MOC_LITERAL(22, 356, 24),
QT_MOC_LITERAL(23, 381, 25),
QT_MOC_LITERAL(24, 407, 4),
QT_MOC_LITERAL(25, 412, 10),
QT_MOC_LITERAL(26, 423, 16),
QT_MOC_LITERAL(27, 440, 7),
QT_MOC_LITERAL(28, 448, 19),
QT_MOC_LITERAL(29, 468, 25),
QT_MOC_LITERAL(30, 494, 5),
QT_MOC_LITERAL(31, 500, 22),
QT_MOC_LITERAL(32, 523, 26),
QT_MOC_LITERAL(33, 550, 15),
QT_MOC_LITERAL(34, 566, 9),
QT_MOC_LITERAL(35, 576, 26),
QT_MOC_LITERAL(36, 603, 25)
    },
    "MyGameClient\0signalHandleGameUpdate\0"
    "\0gp_game_update&\0update\0signalHandleDSQAnswer\0"
    "gp_default_server_query_answer&\0ans\0"
    "signalConnectedToLobby\0myID\0"
    "signalConnectedToGame\0connectToLobbyServer\0"
    "address\0port\0connectToGameServer\0"
    "disconnectFromLobbyServer\0"
    "disconnectFromGameServer\0setNewPlayerName\0"
    "newName\0handleLobbyConnectRequestAnswer\0"
    "gp_connect_answer&\0answer\0"
    "handleConnectButtonClick\0"
    "handleJoinGameButtonClick\0addr\0"
    "gameNumber\0handleKeyPressed\0keyCode\0"
    "handleErrorResponse\0gp_client_error_response&\0"
    "error\0handleGameUpdatePacket\0"
    "handleJoinGameAnswerPacket\0gp_join_answer&\0"
    "updateDSQ\0readyToReadFromLobbyServer\0"
    "readyToReadFromGameServer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MyGameClient[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       5,    1,  112,    2, 0x06 /* Public */,
       8,    1,  115,    2, 0x06 /* Public */,
      10,    0,  118,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    2,  119,    2, 0x0a /* Public */,
      14,    2,  124,    2, 0x0a /* Public */,
      15,    0,  129,    2, 0x0a /* Public */,
      16,    0,  130,    2, 0x0a /* Public */,
      17,    1,  131,    2, 0x0a /* Public */,
      19,    1,  134,    2, 0x0a /* Public */,
      22,    2,  137,    2, 0x0a /* Public */,
      23,    2,  142,    2, 0x0a /* Public */,
      26,    1,  147,    2, 0x0a /* Public */,
      28,    1,  150,    2, 0x0a /* Public */,
      31,    1,  153,    2, 0x0a /* Public */,
      32,    1,  156,    2, 0x0a /* Public */,
      34,    0,  159,    2, 0x0a /* Public */,
      35,    0,  160,    2, 0x08 /* Private */,
      36,    0,  161,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::UInt,    9,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   12,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   12,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   24,   25,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void, 0x80000000 | 29,   30,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 33,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MyGameClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MyGameClient *_t = static_cast<MyGameClient *>(_o);
        switch (_id) {
        case 0: _t->signalHandleGameUpdate((*reinterpret_cast< gp_game_update(*)>(_a[1]))); break;
        case 1: _t->signalHandleDSQAnswer((*reinterpret_cast< gp_default_server_query_answer(*)>(_a[1]))); break;
        case 2: _t->signalConnectedToLobby((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 3: _t->signalConnectedToGame(); break;
        case 4: _t->connectToLobbyServer((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->connectToGameServer((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->disconnectFromLobbyServer(); break;
        case 7: _t->disconnectFromGameServer(); break;
        case 8: _t->setNewPlayerName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->handleLobbyConnectRequestAnswer((*reinterpret_cast< gp_connect_answer(*)>(_a[1]))); break;
        case 10: _t->handleConnectButtonClick((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->handleJoinGameButtonClick((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 12: _t->handleKeyPressed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->handleErrorResponse((*reinterpret_cast< gp_client_error_response(*)>(_a[1]))); break;
        case 14: _t->handleGameUpdatePacket((*reinterpret_cast< gp_game_update(*)>(_a[1]))); break;
        case 15: _t->handleJoinGameAnswerPacket((*reinterpret_cast< gp_join_answer(*)>(_a[1]))); break;
        case 16: _t->updateDSQ(); break;
        case 17: _t->readyToReadFromLobbyServer(); break;
        case 18: _t->readyToReadFromGameServer(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MyGameClient::*_t)(gp_game_update & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MyGameClient::signalHandleGameUpdate)) {
                *result = 0;
            }
        }
        {
            typedef void (MyGameClient::*_t)(gp_default_server_query_answer & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MyGameClient::signalHandleDSQAnswer)) {
                *result = 1;
            }
        }
        {
            typedef void (MyGameClient::*_t)(uint );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MyGameClient::signalConnectedToLobby)) {
                *result = 2;
            }
        }
        {
            typedef void (MyGameClient::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MyGameClient::signalConnectedToGame)) {
                *result = 3;
            }
        }
    }
}

const QMetaObject MyGameClient::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MyGameClient.data,
      qt_meta_data_MyGameClient,  qt_static_metacall, 0, 0}
};


const QMetaObject *MyGameClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MyGameClient::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MyGameClient.stringdata))
        return static_cast<void*>(const_cast< MyGameClient*>(this));
    return QObject::qt_metacast(_clname);
}

int MyGameClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void MyGameClient::signalHandleGameUpdate(gp_game_update & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MyGameClient::signalHandleDSQAnswer(gp_default_server_query_answer & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MyGameClient::signalConnectedToLobby(uint _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MyGameClient::signalConnectedToGame()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
