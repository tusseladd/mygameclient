# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )
# The set of files for implicit dependencies of each language:

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "QT_CORE_LIB"
  "QT_NETWORK_LIB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtQuick"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include/QtQuick"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtQml"
  "C:/qt-everywhere-opensource-src-5.3.1/qtdeclarative/include/QtQml"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtNetwork"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtCore"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/mkspecs/win32-g++"
  "C:/qt-everywhere-opensource-src-5.3.1/qtbase/include/QtGui"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
