/****************************************************************************
** Meta object code from reading C++ file 'serverinfo.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Projects/MyGameClient/serverinfo.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serverinfo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ServerInfo_t {
    QByteArrayData data[14];
    char stringdata[190];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ServerInfo_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ServerInfo_t qt_meta_stringdata_ServerInfo = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 22),
QT_MOC_LITERAL(2, 34, 0),
QT_MOC_LITERAL(3, 35, 18),
QT_MOC_LITERAL(4, 54, 15),
QT_MOC_LITERAL(5, 70, 15),
QT_MOC_LITERAL(6, 86, 16),
QT_MOC_LITERAL(7, 103, 18),
QT_MOC_LITERAL(8, 122, 15),
QT_MOC_LITERAL(9, 138, 11),
QT_MOC_LITERAL(10, 150, 8),
QT_MOC_LITERAL(11, 159, 8),
QT_MOC_LITERAL(12, 168, 9),
QT_MOC_LITERAL(13, 178, 11)
    },
    "ServerInfo\0gameConnectPortChanged\0\0"
    "playerCountChanged\0maxSlotsChanged\0"
    "mapWidthChanged\0mapHeightChanged\0"
    "playerScoreChanged\0gameConnectPort\0"
    "playerCount\0maxSlots\0mapWidth\0mapHeight\0"
    "playerScore"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ServerInfo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       6,   50, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,
       6,    0,   48,    2, 0x06 /* Public */,
       7,    0,   49,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       8, QMetaType::UInt, 0x00495103,
       9, QMetaType::UInt, 0x00495103,
      10, QMetaType::UInt, 0x00495103,
      11, QMetaType::Double, 0x00495103,
      12, QMetaType::Double, 0x00495103,
      13, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

       0        // eod
};

void ServerInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ServerInfo *_t = static_cast<ServerInfo *>(_o);
        switch (_id) {
        case 0: _t->gameConnectPortChanged(); break;
        case 1: _t->playerCountChanged(); break;
        case 2: _t->maxSlotsChanged(); break;
        case 3: _t->mapWidthChanged(); break;
        case 4: _t->mapHeightChanged(); break;
        case 5: _t->playerScoreChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::gameConnectPortChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::playerCountChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::maxSlotsChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::mapWidthChanged)) {
                *result = 3;
            }
        }
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::mapHeightChanged)) {
                *result = 4;
            }
        }
        {
            typedef void (ServerInfo::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ServerInfo::playerScoreChanged)) {
                *result = 5;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject ServerInfo::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ServerInfo.data,
      qt_meta_data_ServerInfo,  qt_static_metacall, 0, 0}
};


const QMetaObject *ServerInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ServerInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ServerInfo.stringdata))
        return static_cast<void*>(const_cast< ServerInfo*>(this));
    return QObject::qt_metacast(_clname);
}

int ServerInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< uint*>(_v) = gameConnectPort(); break;
        case 1: *reinterpret_cast< uint*>(_v) = playerCount(); break;
        case 2: *reinterpret_cast< uint*>(_v) = maxSlots(); break;
        case 3: *reinterpret_cast< double*>(_v) = mapWidth(); break;
        case 4: *reinterpret_cast< double*>(_v) = mapHeight(); break;
        case 5: *reinterpret_cast< int*>(_v) = playerScore(); break;
        default: break;
        }
        _id -= 6;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setGameConnectPort(*reinterpret_cast< uint*>(_v)); break;
        case 1: setPlayerCount(*reinterpret_cast< uint*>(_v)); break;
        case 2: setMaxSlots(*reinterpret_cast< uint*>(_v)); break;
        case 3: setMapWidth(*reinterpret_cast< double*>(_v)); break;
        case 4: setMapHeight(*reinterpret_cast< double*>(_v)); break;
        case 5: setPlayerScore(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
        _id -= 6;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ServerInfo::gameConnectPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ServerInfo::playerCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void ServerInfo::maxSlotsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void ServerInfo::mapWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void ServerInfo::mapHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ServerInfo::playerScoreChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
