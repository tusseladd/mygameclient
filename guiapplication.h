#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include "window.h"
#include "mygameclient.h"

#include <QGuiApplication>

class GuiApplication : public QGuiApplication
{
    Q_OBJECT
public:
    explicit GuiApplication( int& argc, char** argv);

    void initialize();

private:
    Window _window;
    MyGameClient _client;

};

#endif // GUIAPPLICATION_H
