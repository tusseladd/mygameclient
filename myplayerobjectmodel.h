#ifndef MYPLAYEROBJECTMODEL_H
#define MYPLAYEROBJECTMODEL_H

#include <QObject>
#include <QTransform>
#include <QMatrix>
#include <QAbstractListModel>

#include <algorithm>
#include "gameprotocol.h"


namespace PrivateModels{
    // Struct for holding player objects
    struct PlayerData{
        explicit PlayerData():playerID{},position{},isEnemy{}{}
        explicit PlayerData(const int &id, QPoint &pos, bool enemy):
            playerID{id},position{pos},isEnemy{enemy}{}

        int playerID;
        //QString playerName;
        QPoint position;
        bool isEnemy;
    };
}
// Model class for player objects
class MyPlayerObjectModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum PlayerDataRoles:int{
        PlayerIDRole = Qt::UserRole+1,
        //PlayerNameRole,
        PlayerPositionRole,
        PlayerIsEnemyRole
    };
    explicit MyPlayerObjectModel(QObject *parent = 0);
    explicit MyPlayerObjectModel(gp_game_object gameobject,bool isEnemy,
                               QObject *parent = 0);
    ~MyPlayerObjectModel();

    QHash<int,QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    void updatePlayerObjects(const gp_game_object &obj, bool isEnemy);
    void movePlayerObject(const int &objID, const QPoint dir);
    void removePlayer(const int &objID);

signals:
    void signalPlayerShot(const gp_game_object &obj);

private:
    int findPlayer(const int &objID);
    QVector<PrivateModels::PlayerData> _players;

};

#endif // MYPLAYEROBJECTMODEL_H
